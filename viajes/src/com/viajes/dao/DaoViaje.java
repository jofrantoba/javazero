/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.dao;

import com.viajes.beans.Viaje;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author root
 */
public class DaoViaje {
    
    public boolean insertViaje(Viaje objetoInsert){
        Datastore.dbViajes.add(objetoInsert);
        return true;
    }
    
    public boolean updateViaje(Viaje objetoRemove,Viaje objetoUpdate){
        removeViaje(objetoRemove);
        insertViaje(objetoUpdate);
        return true;
    }
    
    public boolean updateViaje(Viaje objetoUpdate){
        Iterator<Viaje> iterador=Datastore.dbViajes.iterator();
        while(iterador.hasNext()){
            Viaje bean=iterador.next();
            if(bean.getIdViaje()==objetoUpdate.getIdViaje()){                
                bean.setLugarPartida(objetoUpdate.getLugarPartida());
                bean.setLugarLlegada(objetoUpdate.getLugarLlegada());
                bean.setDuracion(objetoUpdate.getDuracion());
                break;
            }
        }
        return true;
    }
    
    public boolean removeViaje(Viaje objetoRemove){
        Datastore.dbViajes.remove(objetoRemove);        
        return true;
    }
    
    public boolean removeViaje(int index){
        Datastore.dbViajes.remove(index);        
        return true;
    }
    
    public List<Viaje> listarViaje(){
        return Datastore.dbViajes;
    }
    
    
}
