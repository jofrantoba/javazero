/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.logic;

import com.viajes.beans.Viaje;
import com.viajes.dao.DaoViaje;
import java.util.List;

/**
 *
 * @author root
 */
public class LogicViaje {
    
    public boolean insertViaje(Viaje objetoInsert){
        DaoViaje daoViaje=new DaoViaje();
        return daoViaje.insertViaje(objetoInsert);        
    }
    
    public boolean updateViaje(Viaje objetoRemove,Viaje objetoUpdate){
        DaoViaje daoViaje=new DaoViaje();
        return daoViaje.updateViaje(objetoRemove,objetoUpdate);
    }
    
    public boolean updateViaje(Viaje objetoUpdate){
        DaoViaje daoViaje=new DaoViaje();
        return daoViaje.updateViaje(objetoUpdate);
    }
    
    public boolean removeViaje(Viaje objetoRemove){
        DaoViaje daoViaje=new DaoViaje();
        return daoViaje.removeViaje(objetoRemove);
    }
    
    public boolean removeViaje(int index){
        DaoViaje daoViaje=new DaoViaje();
        return daoViaje.removeViaje(index);
    }
    
    public List<Viaje> listarViaje(){
        DaoViaje daoViaje=new DaoViaje();
        return daoViaje.listarViaje();
    }
}
