/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.process;

import com.viajes.beans.Viaje;
import com.viajes.dao.DaoViaje;
import com.viajes.logic.LogicViaje;
import java.util.List;

/**
 *
 * @author root
 */
public class GestionViaje {
    public final static String INSERT="I";
    public final static String DELETE="D";
    public final static String UPDATE="U";
    
    public boolean mantenimientoViaje(String tipoOperacion,Viaje objeto){
        LogicViaje logic=new LogicViaje();
        if(tipoOperacion.equals(GestionViaje.INSERT)){
            return logic.insertViaje(objeto);
        }else if(tipoOperacion.equals(GestionViaje.UPDATE)){
            return logic.updateViaje(objeto);
        }else if(tipoOperacion.equals(GestionViaje.DELETE)){
            return logic.removeViaje(objeto);
        }
        return false;
    }
    
    public boolean updateViaje(Viaje objetoRemove,Viaje objetoUpdate){
        LogicViaje logic=new LogicViaje();
        return logic.updateViaje(objetoRemove,objetoUpdate);
    }
    
    public boolean removeViaje(int index){
        LogicViaje logic=new LogicViaje();
        return logic.removeViaje(index);
    }
    
    public List<Viaje> listarViaje(){
        LogicViaje logicViaje=new LogicViaje();
        return logicViaje.listarViaje();
    }
}
