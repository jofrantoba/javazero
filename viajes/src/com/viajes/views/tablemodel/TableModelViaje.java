/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.views.tablemodel;

import com.viajes.beans.Viaje;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author root
 */
public class TableModelViaje extends AbstractTableModel{
    private List<Viaje> data;
    private List<String> listColumnHeader;
        
    public TableModelViaje(){        
    }
    
    public void setData(List<Viaje> data){
        this.data=data;
        this.fireTableDataChanged();
    }
    
    public void setHeader(List<String> listColumnHeader){
        this.listColumnHeader=listColumnHeader;
        this.fireTableStructureChanged();
    }  
    
    public Viaje getValue(int rowIndex) {
        try {
            return data.get(rowIndex);
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public int getRowCount() {
        if(data==null){
            return 0;
        }else{
            return data.size();
        }
    }

    @Override
    public int getColumnCount() {
        if(listColumnHeader==null){
            return 0;
        }else{
            return listColumnHeader.size();
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return listColumnHeader.get(0);
            case 1:
                return listColumnHeader.get(1);
            case 2:
                return listColumnHeader.get(2);
            case 3:
                return listColumnHeader.get(3);
            default:
                return "";
            
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return data.get(rowIndex).getIdViaje();
            case 1:
                return data.get(rowIndex).getLugarPartida();
            case 2:
                return data.get(rowIndex).getLugarLlegada();
            case 3:
                return data.get(rowIndex).getDuracion();
            default:
                return "";
        }
    }
    
}
