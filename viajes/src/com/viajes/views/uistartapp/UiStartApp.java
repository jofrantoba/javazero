/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.views.uistartapp;

import com.viajes.views.uiviaje.UiViaje;
import com.viajes.views.uiviaje.UiViajeImpl;
import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 *
 * @author root
 */
public class UiStartApp extends JFrame{
    private UiViaje uiViaje;
    
    public UiStartApp(){
        initComponents();        
    }
    
    private void initComponents(){
        uiViaje=new UiViajeImpl();
        this.getContentPane().add(uiViaje);
    }
    
    public static void main(String arg[]){
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run() {
                UiStartApp ui=new UiStartApp();
                ui.setTitle("Viajes");
                ui.setExtendedState(JFrame.MAXIMIZED_BOTH);
                ui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                ui.setVisible(true);
            }
        });
        
    }
    
}
