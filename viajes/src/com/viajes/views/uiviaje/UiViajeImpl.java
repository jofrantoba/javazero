/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.views.uiviaje;

import com.viajes.beans.Viaje;
import com.viajes.dao.Datastore;
import com.viajes.process.GestionViaje;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class UiViajeImpl extends UiViaje{    
    
    public UiViajeImpl(){
        listarViaje();
    }
    
    @Override
    public boolean isValidaData() {
        return true;
    }
    
    @Override
    public void cleanForm() {
        uiFormViaje.txtId.setText(null);
        uiFormViaje.txtLugarPartida.setText(null);
        uiFormViaje.txtLugarLlegada.setText(null);
        uiFormViaje.txtDuracion.setText(null);
        uiFormViaje.setModoOperacion(UiFormViaje.MODOINSERT);
    }
    
    @Override
    public void processInsertViaje() {
        process(GestionViaje.INSERT);
    }

    @Override
    public void processUpdateViaje() {
        process(GestionViaje.UPDATE);
    }

    @Override
    public void processDeleteViaje() {
        process(GestionViaje.DELETE);
    }
    
    @Override
    public void loadUpdateData() {
       loadFormData(UiFormViaje.MODOUPDATE);
    }

    @Override
    public void loadDeleteData() {
        loadFormData(UiFormViaje.MODODELETE);
    }        

    @Override
    public void listarViaje() {
        uiGridViaje.modelTable.setData(Datastore.dbViajes);        
    }
    
    private void loadFormData(String modoForm){
        int rowTable=uiGridViaje.tblViaje.getSelectedRow();
       if(rowTable==-1){
           JOptionPane.showMessageDialog(null, "Seleccione un registro del grid");
       }else{
           int rowModel=uiGridViaje.tblViaje.convertRowIndexToModel(rowTable);
           Viaje bean=uiGridViaje.modelTable.getValue(rowModel);
           uiFormViaje.txtId.setText(String.valueOf(bean.getIdViaje()));
           uiFormViaje.txtLugarPartida.setText(bean.getLugarPartida());
           uiFormViaje.txtLugarLlegada.setText(bean.getLugarLlegada());
           uiFormViaje.txtDuracion.setText(String.valueOf(bean.getDuracion()));
           uiFormViaje.setModoOperacion(modoForm);
       }
    }
    
    private void process(String operacion){
        GestionViaje gestion=new GestionViaje();
        Viaje objeto=new Viaje();
        int id=Integer.parseInt(uiFormViaje.txtId.getText());
        String lugarPartida=uiFormViaje.txtLugarPartida.getText();
        String lugarLlegada=uiFormViaje.txtLugarLlegada.getText();
        double duracion=Double.parseDouble(uiFormViaje.txtDuracion.getText());
        objeto.setIdViaje(id);
        objeto.setLugarPartida(lugarPartida);
        objeto.setLugarLlegada(lugarLlegada);
        objeto.setDuracion(duracion);        
        gestion.mantenimientoViaje(operacion, objeto);
        if(operacion.equals(GestionViaje.INSERT)){
            JOptionPane.showMessageDialog(null,"INSERTADO CORRECTAMENTE");
        }else if(operacion.equals(GestionViaje.UPDATE)){
            JOptionPane.showMessageDialog(null,"ACTUALIZADO CORRECTAMENTE");
        }else if(operacion.equals(GestionViaje.DELETE)){
            JOptionPane.showMessageDialog(null,"ELIMINADO CORRECTAMENTE");
        }
        uiGridViaje.modelTable.fireTableDataChanged();
    }
    
}
