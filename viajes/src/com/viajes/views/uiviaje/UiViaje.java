/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.views.uiviaje;

import java.awt.BorderLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author root
 */
public class UiViaje extends JPanel implements InterUiViaje{
    protected UiFormViaje uiFormViaje;
    protected UiGridViaje uiGridViaje;
    protected UiToolBarViaje uiToolBarViaje;
    
    public UiViaje(){
        initComponents();
    }
    
    private void initComponents(){
        uiFormViaje=new UiFormViaje(this);
        uiFormViaje.setModoOperacion(UiFormViaje.MODOINSERT);
        uiGridViaje=new UiGridViaje(this);
        this.setLayout(new BorderLayout());
        JPanel pnlFormViaje=new JPanel();
        pnlFormViaje.add(uiFormViaje);
        uiToolBarViaje=new UiToolBarViaje(this);
        JPanel pnlContenido=new JPanel();
        pnlContenido.setLayout(new BorderLayout());
        pnlContenido.add(uiToolBarViaje,BorderLayout.NORTH);
        pnlContenido.add(uiGridViaje,BorderLayout.CENTER);
        this.add(pnlFormViaje,BorderLayout.WEST);
        this.add(pnlContenido,BorderLayout.CENTER);
    }

    @Override
    public void processInsertViaje() {
    }

    @Override
    public void processUpdateViaje() {
    }

    @Override
    public void processDeleteViaje() {
    }

    @Override
    public void listarViaje() {        
    }

    @Override
    public boolean isValidaData() {
        return true;
    }

    @Override
    public void cleanForm() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cargarCombo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadUpdateData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadDeleteData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
