/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.views.uiviaje;

import com.viajes.views.tablemodel.TableModelViaje;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author root
 */
public class UiGridViaje extends JPanel{
    private JScrollPane scrollPane;
    public JTable tblViaje;
    public TableModelViaje modelTable;
    private List<String> listColumnHeader;
    private UiViaje objPadre;
    
    public UiGridViaje(UiViaje objPadre){
        this.objPadre=objPadre;
        initComponents();
        //objPadre.listarViaje();
    }
    
    private void initComponents(){
        scrollPane=new JScrollPane();
        tblViaje=new JTable();
        tblViaje.setAutoCreateRowSorter(true);
        modelTable=new TableModelViaje();      
        tblViaje.setModel(modelTable);  
        listColumnHeader=new ArrayList();
        listColumnHeader.add("ID");
        listColumnHeader.add("LUGAR DE PARTIDA");
        listColumnHeader.add("LUGAR DE LLEGADA");
        listColumnHeader.add("DURACION");
        modelTable.setHeader(listColumnHeader);        
        scrollPane.setViewportView(tblViaje);
        this.setLayout(new BorderLayout() );
        this.add(scrollPane);
    }
}
