/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.views.uiviaje;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JToolBar;

/**
 *
 * @author root
 */
public class UiToolBarViaje extends JToolBar implements ActionListener{
    private JButton btnNuevo;
    private JButton btnCancelar;
    private JButton btnEliminar;
    private JButton btnModificar;
    private UiViaje objPadre;
    
    public UiToolBarViaje(UiViaje objPadre){
        this.objPadre=objPadre;
        initComponents();
        initListener();
    }
    
    private void initComponents(){
        btnNuevo=new JButton("NUEVO");
        btnCancelar=new JButton("CANCELAR");
        btnEliminar=new JButton("ELIMINAR");
        btnModificar=new JButton("MODIFICAR");
        this.add(btnNuevo);
        this.add(btnModificar);
        this.add(btnEliminar); 
        this.add(btnCancelar);               
    }
    
    private void initListener(){
        btnNuevo.addActionListener(this);
        btnModificar.addActionListener(this);
        btnEliminar.addActionListener(this);
        btnCancelar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(btnNuevo) && objPadre!=null){
            objPadre.cleanForm();
        }else if(e.getSource().equals(btnModificar) && objPadre!=null){
            objPadre.loadUpdateData();
        }else if(e.getSource().equals(btnEliminar) && objPadre!=null){
            objPadre.loadDeleteData();
        }else if(e.getSource().equals(btnCancelar) && objPadre!=null){
            //objPadre.cleanForm();
        }
    }
}
