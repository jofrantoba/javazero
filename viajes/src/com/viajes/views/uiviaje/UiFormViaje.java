/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.views.uiviaje;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author root
 */
public class UiFormViaje extends JPanel implements ActionListener{
    public static final String MODOINSERT="AGREGAR";
    public static final String MODOUPDATE="ACTUALIZAR";
    public static final String MODODELETE="ELIMINAR";    
    private JLabel lblId;
    private JLabel lblLugarPartida;
    private JLabel lblLugarLlegada;
    private JLabel lblDuracion;
    public JTextField txtId;
    public JTextField txtLugarPartida;
    public JTextField txtLugarLlegada;
    public JTextField txtDuracion;
    private JButton btnOperacion;
    private String modoOperacion;
    private UiViaje objPadre;
    
    public UiFormViaje(UiViaje objPadre){
        this.objPadre=objPadre;
        initComponents();
        initListener();
    }
    
    public UiFormViaje(){
        initComponents();
        initListener();
    }
    
    private void initComponents(){
    lblId=new JLabel("ID");
    lblLugarPartida=new JLabel("LUGAR PARTIDA");
    lblLugarLlegada=new JLabel("LUGAR LLEGADA");
    lblDuracion=new JLabel("DURACION");
    txtId=new JTextField();
    txtId.setColumns(10);
    txtLugarPartida=new JTextField();
    txtLugarPartida.setColumns(10);
    txtLugarLlegada=new JTextField();
    txtLugarLlegada.setColumns(10);
    txtDuracion=new JTextField();
    txtDuracion.setColumns(10);
    btnOperacion=new JButton("OPERACION");
    GridLayout gridLayout=new GridLayout(4,2,5,5);
    JPanel pnlCenter=new JPanel();
    pnlCenter.setLayout(gridLayout);
    pnlCenter.add(lblId);
    pnlCenter.add(txtId);
    pnlCenter.add(lblLugarPartida);
    pnlCenter.add(txtLugarPartida);
    pnlCenter.add(lblLugarLlegada);
    pnlCenter.add(txtLugarLlegada);
    pnlCenter.add(lblDuracion);    
    pnlCenter.add(txtDuracion);
    JPanel pnlSur=new JPanel();
    pnlSur.add(btnOperacion);
    BorderLayout borderLayout=new BorderLayout(5,5);
    this.setLayout(borderLayout);
    this.add(pnlCenter,BorderLayout.CENTER);
    this.add(pnlSur,BorderLayout.SOUTH);
    }    
    
    private void initListener(){
        btnOperacion.addActionListener(this);
    }
    

    public void setModoOperacion(String modoOperacion) {
        this.modoOperacion = modoOperacion;
        btnOperacion.setText(modoOperacion);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(btnOperacion) && objPadre!=null){
            if(modoOperacion.equals(MODOINSERT)){
                if(objPadre.isValidaData()){
                objPadre.processInsertViaje();
                objPadre.cleanForm();
                }
            }else if(modoOperacion.equals(MODOUPDATE)){
                if(objPadre.isValidaData()){
                objPadre.processUpdateViaje();                
                objPadre.cleanForm();
                }
            }else if(modoOperacion.equals(MODODELETE)){
                if(objPadre.isValidaData()){
                objPadre.processDeleteViaje();
                objPadre.cleanForm();
                }
            }else{
                JOptionPane.showMessageDialog(null, "NO SE RECONOCE OPERACION");
            }
        }
    }
    
    
    
}
