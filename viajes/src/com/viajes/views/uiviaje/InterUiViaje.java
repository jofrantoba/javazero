/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viajes.views.uiviaje;

/**
 *
 * @author root
 */
public interface InterUiViaje {
    void processInsertViaje();
    void processUpdateViaje();
    void processDeleteViaje();
    void loadUpdateData();
    void loadDeleteData();
    void listarViaje();
    boolean isValidaData();
    void cleanForm();
    void cargarCombo();
}
